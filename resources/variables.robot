*** Variables ***

${BASE_URL}  https://demo.playground-crm.com/    # Base URL for CRM system

${PRODUCT_NAME}    J.A.R.V.I.S.                  # Product name

${PRODUCT_QUANTITY}     9                         # Quantity of the product

${CUSTOMER_CARD}     56                           # Customer's card number

${PRODUCT_SHTRIH}  1456                           # Product reference number

${PRODUCT_PRICE}  100                             # Product price

${TIME_TARIF_VALUE}  1                            # Hourly tariff value

${CUSTOMER_NAME}       Tony                       # Customer's first name

${CUSTOMER_SURNAME}    Stark                     # Customer's last name

${CUSTOMER_PHONE}      4563338287                # Customer's phone number

${CUSTOMER_EMAIL}      ironman@avengers.org      # Customer's email address

${USERNAME}            admin                     # Username for logging into the system

${PASSWORD}            admin                     # Password for logging into the system

${WRONG_PASSWORD}      wrongpassword             # Incorrect password
