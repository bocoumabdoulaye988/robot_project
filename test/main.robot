*** Settings ***
Resource    ../resources/variables.robot
Library     SeleniumLibrary

*** Test Cases ***

# Test case to validate login with invalid credentials
Test_Login_With_Invalid_Credentials
    Open Browser    ${BASE_URL}    Chrome
    Input Text    user   ${USERNAME}    # Inputting username
    Sleep    2s
    Input Text    password  ${WRONG_PASSWORD}   # Inputting wrong password
    Sleep   3s
    Click Element    //button[@class="large color blue button"]   # Clicking login button
    Sleep    3s
    Close Browser

# Test case to login and add a product
Test_Login_And_Add_Product
    Open Browser    ${BASE_URL}    Chrome
    Input Text    user   ${USERNAME}    # Inputting username
    Sleep    1s
    Input Text    password   ${PASSWORD}    # Inputting password
    Sleep   1s
    Click Element    //button[@class="large color blue button"]    # Clicking login button
    Sleep   1s
    Click Element    css:li > a > span.icon-myspace    # Clicking on user profile
    Sleep   1s
    Click Element    //input[@class="btn btn-primary"]    # Clicking on add customer button
    Sleep   1s
    Input Text    css=#card.form-control    ${CUSTOMER_CARD}    # Inputting customer card number
    Sleep   1s
    Click Element    xpath://input[@class="form-control" and @placeholder="Name"]    # Clicking on name input
    Sleep   1s
    Input Text    xpath://input[@class="form-control" and @placeholder="Name"]    ${CUSTOMER_NAME}    # Inputting customer name
    Sleep   1s
    Click Element    xpath://input[@class="form-control" and @placeholder="Surname"]    # Clicking on surname input
    Sleep   1s
    Input Text    xpath://input[@class="form-control" and @placeholder="Surname"]    ${CUSTOMER_SURNAME}    # Inputting customer surname
    Sleep   1s
    Click Element    xpath://input[@class="form-control" and @placeholder="Phone"]    # Clicking on phone input
    Sleep   1s
    Input Text    xpath://input[@class="form-control" and @placeholder="Phone"]    ${CUSTOMER_PHONE}    # Inputting customer phone number
    Sleep   1s
    Click Element    xpath://input[@class="form-control" and @placeholder="E-mail"]    # Clicking on email input
    Sleep   1s
    Input Text    xpath://input[@class="form-control" and @placeholder="E-mail"]    ${CUSTOMER_EMAIL}    # Inputting customer email
    Sleep   3s
    Click Element    css:button.btn.btn-primary[name="Submit"]    # Clicking on submit button
    Sleep   2s
    Click Element    css:a#neworder    # Clicking on new order button
    Sleep   2s
    Input Text    id=productID    ${CUSTOMER_CARD}    # Inputting product ID
    Sleep   2s
    Click Element    id=inputPassword1    # Clicking on password input
    Sleep   2s
    Input Text    id=inputPassword1    ${PRODUCT_PRICE}    # Inputting product price
    Sleep   2s
    Click Element    id=time_tarif    # Clicking on time tariff input
    Sleep   2s
    Select From List by Value    id=time_tarif    ${TIME_TARIF_VALUE}    # Selecting time tariff value
    Sleep   2s
    Click Element    id=neworderbtn    # Clicking on new order button
    Sleep   3s
    Click Element    xpath://a[contains(text(), 'Menu')]    # Clicking on menu
    Sleep   2s
    Click Element    css=ul.dropdown-menu li:nth-child(2) a    # Clicking on the product menu
    Sleep   2s
    Click Element    //button[contains(text(), 'Add product')]    # Clicking on add product button
    Sleep   2s
    Execute JavaScript    document.getElementById('goodsname').scrollIntoView(true);    # Scrolling to view element
    Sleep   2s
    Click Element    id=goodsname    # Clicking on goods name input
    Sleep   2s
    Input Text    id=goodsname  ${PRODUCT_NAME}    # Inputting product name
    Sleep   2s
    Click Element    id=shtrih    # Clicking on shtrih input
    Input Text    id=shtrih    ${PRODUCT_SHTRIH}    # Inputting shtrih number
    Sleep   1s
    Click Element    name=price    # Clicking on price input
    Input Text    name=price    ${PRODUCT_PRICE}    # Inputting product price
    Sleep   1s
    Click Element    xpath=//select[@name='unit']    # Clicking on unit select
    Sleep   1s
    Select From List by Label    xpath=//select[@name='unit']    pcs.    # Selecting unit
    Sleep   1s
    Click Element    xpath=//select[@name='directory']    # Clicking on directory select
    Sleep   1s
    Select From List by Value    xpath=//select[@name='directory']    0    # Selecting directory
    Sleep   1s
    Click Element    id=count_type    # Clicking on count type select
    Select From List by Value    id=count_type    0    # Selecting count type
    Sleep   2s
    Click Element    //input[@value="Add"]    # Clicking on add button
    Sleep   2s
    Click Link    Main    # Clicking on main link
    Click Element    xpath://table//td[text()='${CUSTOMER_CARD}']/following-sibling::td/a[text()='information']    # Clicking on customer information link
    Sleep   2s
    Click Element    id=filter-input    # Clicking on filter input
    Input Text    id=filter-input    ${PRODUCT_NAME}    # Inputting product name
    Sleep   2s
    Click Element    xpath://td[span='${PRODUCT_NAME}']/following-sibling::td/input[@placeholder='quantity']    # Clicking on quantity input
    Input Text    xpath://td[span='${PRODUCT_NAME}']/following-sibling::td/input[@placeholder='quantity']    ${PRODUCT_QUANTITY}    # Inputting product quantity
    Sleep    2s
    Click Element    xpath://input[@name='submit_add_srv']    # Clicking on submit button
    Sleep    2s
    Scroll Element Into View    xpath://input[@name='Submit']    # Scrolling to view element
    Click Element    xpath://input[@name='Submit']   
